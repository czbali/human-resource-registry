package hu.braininghub.bh06.hrr.dao;

import org.junit.Test;

import hu.braininghub.bh06.hrr.dao.impl.DefaultJobDAO;
import hu.braininghub.bh06.ooptraining.hrr.model.Job;

public class JobDAOTest {
	
	private JobDAO dao = new DefaultJobDAO();
	
	
	@Test
	public void test_Job_Create_Operation_Works_As_Expected() {
		
		Job testJob = new Job(dao.getIDGenerationStrategy().generateId(), "CEO", null, null, null, null);
		
		String id = dao.create(testJob);
		
		org.junit.Assert.assertEquals(new String(testJob.getId()), new String(id));
		
	}
	
	
	
	
	
	

}

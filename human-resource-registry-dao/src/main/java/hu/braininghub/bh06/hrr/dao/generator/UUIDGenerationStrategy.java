package hu.braininghub.bh06.hrr.dao.generator;

import java.util.UUID;

public class UUIDGenerationStrategy implements IdGenerationStrategy<String> {

	public String generateId() {
		return UUID.randomUUID().toString();
	}

}

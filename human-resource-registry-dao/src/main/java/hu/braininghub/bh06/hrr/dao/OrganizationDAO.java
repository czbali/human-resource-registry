package hu.braininghub.bh06.hrr.dao;

import hu.braininghub.bh06.ooptraining.hrr.model.Organization;

public interface OrganizationDAO extends BaseDAO<String,Organization> {
	
	Organization getOrganizationByName(String name);

}

package hu.braininghub.bh06.hrr.service;

public interface EmployeeService {
	
	void registerNewEmployee(String firstname, String lastname, String birthDate, 
			String identityCardNumber, String jobTitle, String departmentName, Integer salary);

}

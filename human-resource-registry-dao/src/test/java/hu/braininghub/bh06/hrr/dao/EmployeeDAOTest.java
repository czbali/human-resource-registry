package hu.braininghub.bh06.hrr.dao;

import org.junit.Test;

import hu.braininghub.bh06.hrr.dao.impl.DefaultEmployeeDAO;
import hu.braininghub.bh06.ooptraining.hrr.model.Employee;

public class EmployeeDAOTest {
	
private EmployeeDAO dao = new DefaultEmployeeDAO();
	
	@Test
	public void test_Employee_Create_Operation_Works_As_Expected() {
		
		Employee testEmployee = new Employee(dao.getIDGenerationStrategy().generateId(), "1988.06.22");
		testEmployee.setFirstName("Balázs");
		testEmployee.setLastName("Czeglédi");
		
		String id = dao.create(testEmployee);
		
		org.junit.Assert.assertEquals(new String(testEmployee.getId()), new String(id));
		
		Employee loadedEmployee = dao.getById(id);
		
		org.junit.Assert.assertNotNull(loadedEmployee);
		
		org.junit.Assert.assertEquals(testEmployee, loadedEmployee);
		
		org.junit.Assert.assertEquals(1, dao.getEntities().size());
		
	}

}

package hu.braininghub.bh06.hrr.dao.generator;

import java.io.Serializable;

public interface IdGenerationStrategy<T extends Serializable> {
	
	T generateId();

}

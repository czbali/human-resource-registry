package hu.braininghub.bh06.ooptraining.hrr.model;

public class Department extends BusinessObject<String>{

	private Organization organization;
	private String name;
	private Integer plannedNumberOfEmployedColleauges;
	private Employee manager;

	public Department(String id, Organization organization) {
		super(id);
		this.organization = organization;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPlannedNumberOfEmployedColleauges() {
		return plannedNumberOfEmployedColleauges;
	}

	public void setPlannedNumberOfEmployedColleauges(Integer plannedNumberOfEmployedColleauges) {
		this.plannedNumberOfEmployedColleauges = plannedNumberOfEmployedColleauges;
	}

	public Organization getOrganization() {
		return organization;
	}
	
	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

}

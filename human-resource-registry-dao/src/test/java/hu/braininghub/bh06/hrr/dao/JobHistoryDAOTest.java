package hu.braininghub.bh06.hrr.dao;

import org.junit.Test;

import hu.braininghub.bh06.hrr.dao.impl.DefaultJobHistoryDAO;
import hu.braininghub.bh06.ooptraining.hrr.model.Job;
import hu.braininghub.bh06.ooptraining.hrr.model.JobHistory;

public class JobHistoryDAOTest {
	
	private JobHistoryDAO dao = new DefaultJobHistoryDAO();
	
	@Test
	public void test_JobHistory_Create_Operation_Works_As_Expected() {
		
		Job job = new Job(dao.getIDGenerationStrategy().generateId(), "CEO", null, null, null, null);
		JobHistory testJobHistory = new JobHistory(dao.getIDGenerationStrategy().generateId(), "2001", job);
		
		String id = dao.create(testJobHistory);
		
		org.junit.Assert.assertEquals(new String(testJobHistory.getId()), new String(id));
	}

}

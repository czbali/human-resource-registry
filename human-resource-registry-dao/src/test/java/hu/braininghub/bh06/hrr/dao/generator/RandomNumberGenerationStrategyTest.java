package hu.braininghub.bh06.hrr.dao.generator;

import static hu.braininghub.bh06.hrr.dao.generator.RandomNumberGenerationStrategy.DEFAULT_RANDOM_GENERATION_UPPER_LIMIT;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class RandomNumberGenerationStrategyTest {

	@Rule
	public final ExpectedException exeption = ExpectedException.none();

	@Test
	public void testRandomNumberGeneratedSuccesfullyWithIntervall1000() {

		IdGenerationStrategy<Integer> s = new RandomNumberGenerationStrategy(1000);

		Integer generatedId = s.generateId();

		assertNotNull("Generated id cannot be 0!", generatedId);

		boolean generatedNumberIsInsideTheIntervall = generatedId > 1 && generatedId <= 1000;

		if (!generatedNumberIsInsideTheIntervall)
			fail("Generated id is not fit to the specified intervall");

		assertTrue(generatedNumberIsInsideTheIntervall);

	}

	@Test
	public void testRandomNumberGenerationFailedWithNull() {

		exeption.equals(IllegalArgumentException.class);
		exeption.expectMessage("Upper limit parameter is mandatory!");

		new RandomNumberGenerationStrategy(null);
	}

	@Test
	public void testRandomNumberGenerationFailedWithSmallerThanUpperLimit() {
		exeption.equals(IllegalArgumentException.class);
		exeption.expectMessage("Upper limit must be between 1 and" + DEFAULT_RANDOM_GENERATION_UPPER_LIMIT);

		new RandomNumberGenerationStrategy(0);
	}

	@Test
	public void testRandomNumberGenerationFailedWithGreaterThanUpperLimit() {
		exeption.equals(IllegalArgumentException.class);
		exeption.expectMessage("Upper limit must be between 1 and" + DEFAULT_RANDOM_GENERATION_UPPER_LIMIT);

		new RandomNumberGenerationStrategy(DEFAULT_RANDOM_GENERATION_UPPER_LIMIT + 1);
	}

}

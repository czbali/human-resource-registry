package hu.braininghub.bh06.hrr.dao.impl;

import hu.braininghub.bh06.hrr.dao.JobHistoryDAO;
import hu.braininghub.bh06.hrr.dao.generator.IdGenerationStrategy;
import hu.braininghub.bh06.hrr.dao.generator.UUIDGenerationStrategy;
import hu.braininghub.bh06.ooptraining.hrr.model.JobHistory;

public class DefaultJobHistoryDAO extends DefaultBaseDAO<String, JobHistory> implements JobHistoryDAO {
	
	public IdGenerationStrategy<String> getIDGenerationStrategy() {
		return new UUIDGenerationStrategy();
	}

}

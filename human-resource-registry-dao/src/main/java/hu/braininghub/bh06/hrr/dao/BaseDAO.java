package hu.braininghub.bh06.hrr.dao;

import java.io.Serializable;
import java.util.List;

import hu.braininghub.bh06.hrr.dao.generator.IdGenerationStrategy;
import hu.braininghub.bh06.ooptraining.hrr.model.BusinessObject;

public interface BaseDAO<ID extends Serializable, T extends BusinessObject<ID>> {
	
	ID create(T object);
	
	void delete(ID id);
	
	void update(T object);
	
	T getById(ID id);
	
	List<T> getEntities();
	
	IdGenerationStrategy<ID> getIDGenerationStrategy();

}

package hu.braininghub.bh06.hrr.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import hu.braininghub.bh06.hrr.dao.impl.DefaultDepartmentDAO;
import hu.braininghub.bh06.ooptraining.hrr.model.Department;
import hu.braininghub.bh06.ooptraining.hrr.model.Organization;

public class DepartmentDAOTest {

	private DepartmentDAO dao;
	
	@Before
	public void init() {
		dao = new DefaultDepartmentDAO();
	}

	@Test
	public void test_Department_Create_Operation_Works_As_Expected() {

		Organization o = new Organization(dao.getIDGenerationStrategy().generateId(), 2000);
		Department testDepartment = new Department(dao.getIDGenerationStrategy().generateId(), o);
		testDepartment.setName("TestDepartment");

		String id = dao.create(testDepartment);

		org.junit.Assert.assertEquals(new String(testDepartment.getId()), new String(id));

		Department loadedDepartment = dao.getById(id);

		org.junit.Assert.assertNotNull(loadedDepartment);

		org.junit.Assert.assertEquals(testDepartment, loadedDepartment);

		org.junit.Assert.assertEquals(1, dao.getEntities().size());
	}
}


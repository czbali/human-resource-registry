package hu.braininghub.bh06.hrr.dao;

import org.junit.Test;

import hu.braininghub.bh06.hrr.dao.impl.DefaultOrganizationDAO;
import hu.braininghub.bh06.ooptraining.hrr.model.Organization;

public class OrganizationDAOTest {
	
	private OrganizationDAO dao = new DefaultOrganizationDAO();
	
	@Test
	public void test_Organization_Create_Operation_Works_As_Expected() {
		
		Organization testOrganization = new Organization(dao.getIDGenerationStrategy().generateId(), 2001);
		
		String id = dao.create(testOrganization);
		
		org.junit.Assert.assertEquals(new String(testOrganization.getId()), new String(id));
		
	}
	
	
	
	

}

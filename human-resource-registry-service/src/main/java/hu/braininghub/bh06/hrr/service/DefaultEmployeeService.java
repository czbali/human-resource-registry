package hu.braininghub.bh06.hrr.service;

import hu.braininghub.bh06.hrr.dao.DepartmentDAO;
import hu.braininghub.bh06.hrr.dao.EmployeeDAO;
import hu.braininghub.bh06.hrr.dao.JobDAO;
import hu.braininghub.bh06.hrr.dao.JobHistoryDAO;
import hu.braininghub.bh06.hrr.dao.impl.DefaultEmployeeDAO;
import hu.braininghub.bh06.hrr.dao.impl.DefaultJobDAO;
import hu.braininghub.bh06.hrr.dao.impl.DefaultJobHistoryDAO;
import hu.braininghub.bh06.hrr.dao.impl.DefaultDepartmentDAO;
import hu.braininghub.bh06.ooptraining.hrr.model.Employee;
import hu.braininghub.bh06.ooptraining.hrr.model.Job;
import hu.braininghub.bh06.ooptraining.hrr.model.Department;

public class DefaultEmployeeService implements EmployeeService {

	private JobDAO jobDAO = new DefaultJobDAO();
	private JobHistoryDAO jobHistoryDAO = new DefaultJobHistoryDAO();
	private EmployeeDAO employeeDAO = new DefaultEmployeeDAO();
	private DepartmentDAO departmentDAO = new DefaultDepartmentDAO();

	private void checkIntegerParameterNotNullOrZero(Integer... args) {

		for (Integer param : args) {
			if (param == null || param.intValue() == 0) {
				throw new IllegalArgumentException("Parameter must be present and greater than 0!");
			}
		}
	}

	private void checkStringParameterNotNullOrEmpty(String... args) {

		for (String s : args) {
			if (s == null || s.isEmpty()) {
				throw new IllegalArgumentException("Parameter must be present and not empty!");
			}
		}
	}

	@Override
	public void registerNewEmployee(String firstname, String lastname, String birthDate, String identityCardNumber,
			String jobTitle, String departmentName, Integer salary) {

		checkStringParameterNotNullOrEmpty(firstname, lastname, birthDate, identityCardNumber, jobTitle,
				departmentName);
		checkIntegerParameterNotNullOrZero(salary);

		Employee e = new Employee(employeeDAO.getIDGenerationStrategy().generateId(), birthDate);
		e.setActive(Boolean.TRUE);

		Job currentJob = jobDAO.getJobByTitleAndDepartmentName(jobTitle, departmentName);

		e.setCurrentJob(currentJob);
		e.setFirstName(firstname);
		e.setLastName(lastname);
		e.setIdentityCardNumber(identityCardNumber);
		e.setSalary(salary);

		employeeDAO.create(e);

	}

}


package hu.braininghub.bh06.hrr.dao;

import org.junit.Test;

import hu.braininghub.bh06.hrr.dao.impl.DefaultJobCategoryDAO;
import hu.braininghub.bh06.ooptraining.hrr.model.JobCategory;

public class JobCategoryDAOTest {
	
	private JobCategoryDAO dao = new DefaultJobCategoryDAO();
	
	@Test
	public void test_JobCategory_Create_Operation_Works_As_Expected() {
		
		JobCategory testJobCategory = new JobCategory(dao.getIDGenerationStrategy().generateId(), "A világ legjobb munkája", 3);
		
		testJobCategory.setMinSalary(8000);
		testJobCategory.setMaxSalary(16000);
		
		String id = dao.create(testJobCategory);
		
		org.junit.Assert.assertEquals(new String(testJobCategory.getId()), new String(id));
		
		
		
	}

}

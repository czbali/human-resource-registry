package hu.braininghub.bh06.hrr.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hu.braininghub.bh06.hrr.dao.BaseDAO;
import hu.braininghub.bh06.ooptraining.hrr.model.BusinessObject;

public abstract class DefaultBaseDAO<ID extends Serializable, T extends BusinessObject<ID>> implements BaseDAO<ID, T> {

	protected Map<ID, T> entities = new HashMap<ID, T>();

	public ID create(T object) {
		
		if(object.getId() == null) {
			throw new IllegalArgumentException("Missing ID!");
		}
		entities.put(object.getId(), object);
		return object.getId();
	}

	public void delete(ID id) {

		T object = entities.get(id);

		if (object == null) {
			throw new IllegalArgumentException("Entity not found with the given id! > " + id);
		}

		object.setActive(Boolean.FALSE);

	}

	public void update(T object) {

		entities.put(object.getId(), object);
	}

	public T getById(ID id) {
		return entities.get(id);
	}

	public List<T> getEntities() {
		return new ArrayList<T>(entities.values());
	}

}

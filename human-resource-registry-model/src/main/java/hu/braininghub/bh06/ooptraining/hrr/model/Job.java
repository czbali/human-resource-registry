package hu.braininghub.bh06.ooptraining.hrr.model;

public class Job extends BusinessObject<String> {

	private String title;
	private Department department;
	private JobCategory category;
	private String description;
	
	public Job(String id, String title, Department department, JobCategory category, Boolean active,
			String description) {
		super(id);
		this.title = title;
		this.department = department;
		this.category = category;
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public Department getDepartment() {
		return department;
	}

	public JobCategory getCategory() {
		return category;
	}

	public String getDescription() {
		return description;
	}
}
